export default (varName) => {
	return getComputedStyle(document.body).getPropertyValue("--" + varName);
};