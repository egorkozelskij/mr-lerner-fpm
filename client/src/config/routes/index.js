import React from 'react';
import {withRouter} from "react-router-dom";

import Error404 from 'error/Error404';
import {AuthPage} from "pages/Auth";
import Dictionaries from "pages/DictionaryList";
import RequireAuth from "template/RequireAuth";

const RequireAuthRouter = withRouter(RequireAuth);

const router = {
	common: [
		{
			path: '/auth',
			component: AuthPage,
			name: 'auth',
			exact: true
		},
		{
			path: '/dictionaries',
			name: ['home', 'dicts'],
			exact: false,
			component: () => (<RequireAuthRouter component={Dictionaries}/>)
		},
	],
	error: {
		component: Error404
	}
};
export default router;

export const routeTo = (name) => {
	const path = router.common.filter((item) => (Array.isArray(item.name) && item.name.includes(name)) || item.name === name);
	return (path === null || path.length === 0) ? '/404' : path[0].path;
};
