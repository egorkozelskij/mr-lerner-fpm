const SIGNIN_REQUEST = 'auth/signInRequest';
const SIGNIN_SUCCESS = 'auth/signInSuccess';
const SIGNIN_FAIL = 'auth/signInFail';

export {
	SIGNIN_REQUEST,
	SIGNIN_SUCCESS,
	SIGNIN_FAIL
};