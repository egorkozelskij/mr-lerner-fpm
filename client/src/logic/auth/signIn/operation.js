import * as actions from './action';
import { setInLocalStorage } from 'libs/webStorage';
import wrapper from 'libs/funcWrapper';

const signInUser = (creds) => {
	return dispatch => {
		dispatch(actions.signInRequest());
		wrapper(() => {
			fetch('/api/auth/signin', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					email: creds.email,
					password: creds.password,
				}),
			})
			.then(res => res.json())
			.then(json => {
				if (json.success) {
					setInLocalStorage('auth', {token: json.token});
					dispatch(actions.signInSuccess(json.user));
				} else {
					dispatch(actions.signInFail(json.message));
				}
			})
			.catch(() => {
				dispatch(actions.signInFail("Unknown error"));
			})
		});
	}
};
export default signInUser;