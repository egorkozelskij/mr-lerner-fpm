import * as types from './type';

export const signInRequest = () => {
	return {
		type: types.SIGNIN_REQUEST,
		payload: {
			isAuth: false,
			isFetching: true
		}
	}
};

export const signInSuccess = (user) => {
	return {
		type: types.SIGNIN_SUCCESS,
		payload: {
			isAuth: true,
			isFetching: false,
			user: user,
			signInSuccess: true
		}
	}
};

export const signInFail = (message) => {
	return {
		type: types.SIGNIN_FAIL,
		payload: {
			isAuth: false,
			isFetching: false,
			signInSuccess: false,
			errorMessage: message
		}
	}
};