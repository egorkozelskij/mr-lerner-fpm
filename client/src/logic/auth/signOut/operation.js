import * as actions from './action';
import { getFromLocalStorage } from 'libs/webStorage';

const signOutUser = () => {
	return dispatch => {
		dispatch(actions.signOutRequest());
		const obj = getFromLocalStorage('auth');
		if (obj)
			localStorage.removeItem('auth');
		
		if (obj && obj.token) {
			fetch('/api/account/signOut?token=' + obj.token)
			.catch(() => {
				console.err("Unknown signOut error");
			});
		}
	}
};
export default signOutUser;