import * as types from './type';


export const verifyRequest = () => {
	return {
		type: types.VERIFY_REQUEST,
		payload: {
			isAuth: false,
			isFetching: true
		}
	}
};


export const verifySuccess= (user) => {
	return {
		type: types.VERIFY_SUCCESS,
		payload: {
			isAuth: true,
			isFetching: false,
			user: user
		}
	}
};

export const verifyFail = () => {
	return {
		type: types.VERIFY_FAIL,
		payload: {
			isAuth: false,
			isFetching: false
		}
	}
};
