import React from 'react';
import PropTypes from 'prop-types';
import Loader from 'react-loader-spinner';

import Dimmer from "bundles/common/Dimmer";
import './style.scss';

const DimmerLoader = (props) => {
	
	const {size, color, type, dimmerColor, dimmerDuration, dimmerFunc, hidden} = props;
	
	let loaderSize = '15%';
	let loaderColor = 'white';
	let loaderType = 'Bars';
	
	if (size)
		loaderSize = size;
	
	if (color)
		loaderColor = color;
	
	if (type)
		loaderType = type;
	
	const style = {
		width: loaderSize,
		height: loaderSize
	};
	
	return (
		<React.Fragment>
			<Dimmer color={dimmerColor} duration={dimmerDuration} func={dimmerFunc} hidden={hidden}>
				<div className='dimmerLoader'>
					<div style={style}>
						<Loader type={loaderType} color={loaderColor}/>
					</div>
				</div>
			</Dimmer>
		</React.Fragment>
	);
};

DimmerLoader.propTypes = {
	size: PropTypes.string,
	color: PropTypes.string,
	type: PropTypes.string,
	dimmerColor: PropTypes.string,
	dimmerDuration: PropTypes.number,
	dimmerFunc: PropTypes.string,
	hidden: PropTypes.bool
};

export default DimmerLoader;
