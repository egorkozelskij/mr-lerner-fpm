import React, {Component} from 'react';

class DimmerLoaderContainer extends Component {
	render() {
		return (
			<div className={'dimmer-container ' + this.props.className}>
				{this.props.children}
			</div>
		);
	}
}
export default DimmerLoaderContainer;
