import React from 'react';
import PropTypes from 'prop-types';

import MDSpinner from "react-md-spinner";

const Loader = ({size,color}) => {
	return (
		<MDSpinner singleColor={color?color:'white'} size={size}/>
	);
};
Loader.propTypes = {
	size: PropTypes.string,
	color: PropTypes.string
};

export default Loader;
