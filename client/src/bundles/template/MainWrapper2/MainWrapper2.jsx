import React from 'react';
import {Container, Row, Col} from "reactstrap";
import PropTypes from 'prop-types';

import './style.scss';

const MainWrapper2 = ({LeftComponent, TopComponent, MiddleComponent}) => (
	<React.Fragment>
		<Container fluid className='page-main-wrapper'>
			<Row>
				<Col className='d-none d-lg-block' lg={3} xl={2}>
					<div className='d-flex justify-content-start'>
						{LeftComponent? <LeftComponent/> : ''}
					</div>
				</Col>
				<Col className='col-12 p-0 offset-lg-1'
				     xs={12} sm={12} md={12} lg={8} xl={7}>
					<Container fluid>
						{TopComponent? <TopComponent/> : ''}
					</Container>
					<Container fluid>
						{MiddleComponent? <MiddleComponent/> : ''}
					</Container>
				</Col>
			</Row>
		</Container>
	</React.Fragment>
 );
MainWrapper2.propTypes = {
	LeftComponent: PropTypes.any,
	TopComponent:  PropTypes.any,
	MiddleComponent:  PropTypes.any
};
export default MainWrapper2;
