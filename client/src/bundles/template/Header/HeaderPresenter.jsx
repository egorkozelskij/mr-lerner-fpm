import React from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem, NavLink
} from 'reactstrap';
import {Link} from "react-router-dom";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";

import Loader from "common/Loader";
import {routeTo} from 'config/routes';
import {signOutUser} from "logic/auth";

import './style.scss';

const HeaderPresenter = ({isAuth, isFetching, signOutUser}) => (
	<nav className="navbar fixed-top navbar-default navbar-expand-lg mb-0" id="header" role="navigation">
		<NavbarBrand href="/">MR LERNER</NavbarBrand>
		<NavbarToggler />
		<Collapse navbar>
			<Nav className="ml-0" navbar>
				<NavItem>
					<Link to={routeTo('dicts')} className='nav-link'>My Dictionaries</Link>
				</NavItem>
				<NavItem>
					<Link to={routeTo('time_to_work')} className='nav-link'>Time to Work!</Link>
				</NavItem>
			</Nav>
			
			<Nav className='ml-auto' navbar>
				{!isFetching ?
					<React.Fragment>
						{(!isAuth) ?
							<React.Fragment>
								<NavItem>
									<Link to={routeTo('auth')} className='nav-link'>Sign in / Sign up</Link>
								</NavItem>
							</React.Fragment>
							:
							<React.Fragment>
								<NavItem>
									<Link to={routeTo('profile')} className='nav-link'>Profile</Link>
								</NavItem>
								<NavItem>
									<a onClick={signOutUser} className='nav-link btn_as_link'>Sign out</a>
								</NavItem>
							</React.Fragment>
						}
					</React.Fragment>
					:
					<Loader size='30px'/>
				}
			</Nav>
		</Collapse>
	</nav>
 );
HeaderPresenter.propTypes = {
	isAuth: PropTypes.bool.isRequired,
	isFetching: PropTypes.bool,
	signOutUser: PropTypes.func.isRequired
};
const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth,
		isFetching: state.auth.isFetching
	}
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({signOutUser}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(HeaderPresenter);

