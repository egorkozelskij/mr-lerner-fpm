import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const TextItem = ({active, title}) => {
	const classes = classNames('d-flex', 'text-item',{ active: active });
	return(
		<div className={classes}>
			{title?title:'Item'}
		</div>
	);
};
TextItem.propTypes = {
	title: PropTypes.string,
	active: PropTypes.bool
};
export default TextItem;
