import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';

const InputItem = ({placeholder, onChange}) => {
	return(
		<div className="input-group input-item">
			<input type="text"
			       className="form-control"
			       placeholder={placeholder}
			       onChange={onChange}/>
		</div>
	);
};
InputItem.propTypes = {
	placeholder: PropTypes.string,
	onChange: PropTypes.func.isRequired
};
export default InputItem;
