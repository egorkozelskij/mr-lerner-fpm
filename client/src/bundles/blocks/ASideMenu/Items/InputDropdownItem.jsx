import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Select2 from 'react-select2-wrapper';



class InputDropdownItem extends Component {
	constructor(props) {
		super(props);
		this.onSelect = this.onSelect.bind(this);
	}
	
	onSelect(obj)  {
		this.props.onSelect(obj.params.data);
	};
	
	render() {
		return (
			<div className="input-group input-dropdown-item">
				<Select2
					data={this.props.data}
					onSelect={this.onSelect}
					options={{
						placeholder: this.props.placeholder,
					}}/>
			</div>
		);
	}
}
InputDropdownItem.propTypes = {
	placeholder: PropTypes.string,
	data: PropTypes.array,
	onSelect: PropTypes.func.isRequired
};
export default InputDropdownItem;
