import React from 'react';
import PropTypes from 'prop-types';
import {Container} from "reactstrap";

import './style.scss';

const ASideMenu = ({children, title}) => (
      <Container fluid className='menu-wrapper d-flex flex-column'>
          <div className='d-flex justify-content-center'>{title?title:'Menu'}</div>
	      {children}
      </Container>
);
ASideMenu.propTypes = {
	title: PropTypes.string
};
export default ASideMenu;
