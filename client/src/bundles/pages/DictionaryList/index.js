import DictionaryList from "./DictionaryList";
import reducer from './logic/reducer';
import * as operations from './logic/operation';

export const dictReducer = reducer;
export const addNewDict = operations.addNewDict;
export default DictionaryList;