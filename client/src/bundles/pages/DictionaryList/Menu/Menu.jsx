import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import MenuPresenter from "./MenuPresenter";
import wrapper from "libs/funcWrapper";
import {countries} from "countries-list";

import {addNewDict} from "pages/DictionaryList";

class Menu extends Component {
	constructor(props) {
		super(props);
		this.state = {
			name: undefined,
			lang: undefined,
			code: undefined,
			langData: [],
			LangLoading: true
		};
		this.onPropChange = this.onPropChange.bind(this);
		this.onSave = this.onSave.bind(this);
		
		wrapper(() => {
			let langData = [];
			Object.keys(countries).forEach((countryCode) => {
				let countryData = countries[countryCode];
				//HARDCODE
					if (countryData.name === 'United Kingdom') {
						langData.push({
							id: countryCode.toLowerCase(),
							text: "England"
						});
						return;
					}
				//
				langData.push({
					id: countryCode.toLowerCase(),
					text: countryData.name
				});
			});
			this.setState({
				...this.state,
				LangLoading: false,
				langData: langData
			});
		});
	}
	
	onSave() {
		this.props.addNewDict({
			name: this.state.name,
			lang: this.state.lang,
			code: this.state.code
		});
	}
	
	onPropChange(prop, value) {
		this.state[prop] = value;
	}
	
	render() {
		return <MenuPresenter data={this.state.langData}
		                      onSave={this.onSave}
		                      onPropChange={this.onPropChange}
		                      LangLoading={this.state.LangLoading}/>
	}
}
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({addNewDict}, dispatch);
};
export default connect(null, mapDispatchToProps)(Menu);

