import React, {Component} from 'react';
import {connect} from "react-redux";
import PropTypes from "prop-types";
import {bindActionCreators} from "redux";

import Card from "./Card";
import {GetDicts} from "./logic/operation";

class CardsWrapper extends Component  {
	componentDidMount() {
		this.props.GetDicts();
	}
	render() {
		return(
			<div className="d-flex flex-wrap justify-content-start">
				{this.props.dicts.map((dict, key) => (
					<Card key={key}
					      name={dict.name}
					      lang={dict.lang}
					      code={dict.code}
					      id={dict._id}/>
				))}
			</div>
		);
	}
}
CardsWrapper.propTypes = {
	dicts: PropTypes.arrayOf(PropTypes.shape({
		name: PropTypes.string.isRequired,
		lang: PropTypes.string.isRequired,
		code: PropTypes.string
	})).isRequired,
	GetDicts: PropTypes.func.isRequired
};
const mapStateToProps = (state) => {
	return {
		dicts: state.dicts.data
	};
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		GetDicts
		}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(CardsWrapper);

