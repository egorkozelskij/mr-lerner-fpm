import * as types from './type';

export const AddDictRequest = () => {
	return {
		type: types.ADDDICT_REQUEST,
		payload: {
			isFetching: true
		}
	}
};

export const AddDictSuccess= (dict) => {
	return {
		type: types.ADDDICT_SUCCESS,
		payload: {
			isFetching: false,
			newDict: dict
		}
	}
};

export const AddDictFail = (message) => {
	return {
		type: types.ADDDICT_FAIL,
		payload: {
			isFetching: false,
			isSuccess: false,
			message: message
		}
	}
};

export const GetDictsRequest = () => {
	return {
		type: types.GETDICTS_REQUEST,
		payload: {
			isFetching: true
		}
	}
};

export const GetDictsSuccess= (dicts) => {
	return {
		type: types.GETDICTS_SUCCESS,
		payload: {
			isFetching: false,
			dicts: dicts
		}
	}
};

export const GetDictsFail = (message) => {
	return {
		type: types.GETDICTS_FAIL,
		payload: {
			isFetching: false,
			isSuccess: false,
			message: message
		}
	}
};