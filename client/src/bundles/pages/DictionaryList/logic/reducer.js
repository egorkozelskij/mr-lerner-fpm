import * as types from './type';

const initialState = {
	// data: [
	// 		{
	// 			name: "Test spanish dictionary",
	// 			lang: 'Spain',
	// 			code: 'es'
	// 		},
	// 		{
	// 			name: "Test english dictionary",
	// 			lang: 'English',
	// 			code: 'gb'
	// 		},
	// 		{
	// 			name: "Test french dictionary",
	// 			lang: 'French',
	// 			code: 'fr'
	// 		}
	// 	],
	data: [],
	isFetching: false
};


function dictReducer(state = initialState, action) {
	switch (action.type) {
		
		case types.ADDDICT_REQUEST:
			return Object.assign({}, state, action.payload);
		
		case types.ADDDICT_SUCCESS:
			return Object.assign({}, {
				isFetching: action.payload.isFetching,
				data: [...state.data, action.payload.newDict]
			});
			
		case types.ADDDICT_FAIL:
			return Object.assign({}, state, action.payload);
			
		case types.GETDICTS_REQUEST:
		case types.GETDICTS_FAIL:
			return Object.assign({}, state, action.payload);
		
		case types.GETDICTS_SUCCESS:
			return Object.assign({}, state, {
				isFetching: action.payload.isFetching,
				data: action.payload.dicts
			});
			
		default:
			return state;
	}
}

export default dictReducer;
