import React, { Component } from 'react';
import PropTypes from 'prop-types';

import AuthPagePresenter from "./AuthPagePresenter";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Redirect} from "react-router";

import {signInUser, signUpUser} from 'logic/auth';
import authConfig from "config/auth";
import DimmerLoaderContainer from "common/DimmerLoader/DimmerLoaderContainer";
import {DimmerLoader} from "common/DimmerLoader";

export const AuthPageTab = Object.freeze({
	SIGNIN:  Symbol("signIn"),
	SIGNUP:  Symbol("signUp")
});

class AuthPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			active: this.props.active === AuthPageTab.SIGNUP ? 1 : 0
		};
		this.signInCreds = {};
		this.signUpCreds = {};
		
		this.onTabChange = this.onTabChange.bind(this);
		this.onSignInEmailChange = this.onSignInEmailChange.bind(this);
		this.onSignInPassChange = this.onSignInPassChange.bind(this);
		this.onSignUpPassChange = this.onSignUpPassChange.bind(this);
		this.onSignUpPassAgainChange = this.onSignUpPassAgainChange.bind(this);
		this.onSignUpEmailChange = this.onSignUpEmailChange.bind(this);
		this.onSignIn = this.onSignIn.bind(this);
		this.onSignUp = this.onSignUp.bind(this);
	}
	
	onTabChange(tabNum) {
		this.setState({
			...this.state,
			active: tabNum
		});
	}
	
	//SIGN IN
	onSignInEmailChange(newEmail) {
		this.signInCreds.email = newEmail;
	}
	
	onSignInPassChange(newPass) {
		this.signInCreds.password = newPass;
	}
	
	//SIGN UP
	onSignUpPassChange(newPass) {
		this.signUpCreds.password = newPass;
	}
	
	onSignUpPassAgainChange(newPass) {
		this.signUpCreds.passwordAgain = newPass;
	}
	
	onSignUpEmailChange(newEmail) {
		this.signUpCreds.email = newEmail;
	}
	
	onSignIn(e) {
		e.preventDefault();
		this.props.signInUser(this.signInCreds);
	}
	
	onSignUp(e) {
		e.preventDefault();
		this.props.signUpUser(this.signUpCreds);
	}
	
    render() {
        if (this.props.isAuth && authConfig.redirectEnable)
			return <Redirect to={authConfig.redirect}  />;
			
	    if (this.props.signUpSuccess)
		    return <Redirect to='/dictionaries' />;
			
		let Dimmer = () => ('');
        if (this.props.isVerifying)
	        Dimmer = DimmerLoader;

        return (
	        <DimmerLoaderContainer>
	        <AuthPagePresenter active={this.state.active}
	                           onTabChange={this.onTabChange}
	                           onSignInEmailChange = {this.onSignInEmailChange}
	                           onSignInPassChange = {this.onSignInPassChange}
	                           onSignUpPassChange = {this.onSignUpPassChange}
	                           onSignUpPassAgainChange = {this.onSignUpPassAgainChange}
	                           onSignUpEmailChange = {this.onSignUpEmailChange}
	                           onSignIn = {this.onSignIn}
	                           onSignUp = {this.onSignUp}
	                           {...this.props}/>
		        <Dimmer/>
	        </DimmerLoaderContainer>
        );
    }
}
AuthPage.propTypes = {
	active: PropTypes.objectOf(AuthPageTab),
	signInUser: PropTypes.func.isRequired,
	signUpUser: PropTypes.func.isRequired,
	isAuth: PropTypes.bool.isRequired,
	isVerifying: PropTypes.bool,
	match: PropTypes.object.isRequired
};
const mapStateToProps = (state) => {
	return {
		isAuth: state.auth.isAuth,
		isVerifying: state.auth.isVerifying,
		signUpSuccess: state.auth.signUpSuccess
	};
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		signInUser,
		signUpUser}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(AuthPage);
