import Dictionary from "./Dictionary";
import reducer from './logic/reducer';

export default Dictionary;
export const wordReducer = reducer;