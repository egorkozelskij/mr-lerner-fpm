import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import AllWorldPresenter from "./AllWorldPresenter";
import {DeleteWord, GetWords, addNewWord} from "../../logic/operation";

class AllWords extends Component {
	constructor(props) {
		super(props);
		this.onDelete = this.onDelete.bind(this);
		this.onCreate = this.onCreate.bind(this);
		this.onChangeW = this.onChangeW.bind(this);
		this.onChangeT = this.onChangeT.bind(this);
		
		this.addW = '';
		this.addT = '';
	}
	
	componentDidMount() {
		this.props.GetWords(this.props.dictId);
	}
	
	onDelete(wordId) {
		this.props.DeleteWord(wordId);
	}
	
	onCreate() {
		if (this.addW === '' || this.addT === '') {
			console.log('Value is empty');
			return;
		}
		this.props.addNewWord(this.props.dictId, {
			value: this.addW,
			translate: this.addT
		});
	}
	
	onChangeW(e) {
		this.addW = e.target.value;
	}
	
	onChangeT(e) {
		this.addT = e.target.value;
	}
	
    render() {
        return (
			<AllWorldPresenter dictId={this.props.dictId} onDelete={this.onDelete} onCreate={this.onCreate}
			                   onChangeW={this.onChangeW}
			                   onChangeT={this.onChangeT}/>
        );
    }
}
AllWords.propTypes = {
	dictId: PropTypes.string.isRequired,
	GetWords: PropTypes.func.isRequired,
	DeleteWord: PropTypes.func.isRequired
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
		GetWords,
		DeleteWord,
		addNewWord
	}, dispatch);
};
export default connect(null, mapDispatchToProps)(AllWords);
