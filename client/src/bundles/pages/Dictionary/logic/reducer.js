import * as types from './type';

const initialState = {
	data: [],
	isFetching: false
};


function wordReducer(state = initialState, action) {
	switch (action.type) {
		
		case types.ADDWORD_REQUEST:
			return Object.assign({}, state, action.payload);
		
		case types.ADDWORD_SUCCESS:
			return Object.assign({}, {
				isFetching: action.payload.isFetching,
				data: [...state.data, action.payload.newWord]
			});
			
		case types.ADDWORD_FAIL:
			return Object.assign({}, state, action.payload);
			
		case types.GETWORDS_REQUEST:
		case types.GETWORDS_FAIL:
			return Object.assign({}, state, action.payload);
		
		case types.GETWORDS_SUCCESS:
			return Object.assign({}, state, {
				isFetching: action.payload.isFetching,
				data: action.payload.words
			});
			
		case types.DELETE_REQUEST:
			return Object.assign({}, state, {
				data: state.data.filter((d) => d._id !== action.payload.wordId)
			});
			
		default:
			return state;
	}
}

export default wordReducer;
