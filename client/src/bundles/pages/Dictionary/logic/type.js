const ADDWORD_REQUEST = 'words/addRequest';
const ADDWORD_SUCCESS = 'words/addSuccess';
const ADDWORD_FAIL = 'words/addFail';

const GETWORDS_REQUEST = 'words/getRequest';
const GETWORDS_SUCCESS = 'words/getSuccess';
const GETWORDS_FAIL = 'words/getFail';

const DELETE_REQUEST = 'words/deleteRequest';

export {
	GETWORDS_REQUEST,
	GETWORDS_SUCCESS,
	GETWORDS_FAIL,
	
	ADDWORD_REQUEST,
	ADDWORD_SUCCESS,
	ADDWORD_FAIL,
	
	DELETE_REQUEST
};