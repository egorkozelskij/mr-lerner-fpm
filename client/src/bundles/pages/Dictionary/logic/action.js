import * as types from './type';

export const AddWordRequest = () => {
	return {
		type: types.ADDWORD_REQUEST,
		payload: {
			isFetching: true
		}
	}
};

export const AddWordSuccess= (word) => {
	return {
		type: types.ADDWORD_SUCCESS,
		payload: {
			isFetching: false,
			newWord: word
		}
	}
};

export const AddWordFail = (message) => {
	return {
		type: types.ADDWORD_FAIL,
		payload: {
			isFetching: false,
			isSuccess: false,
			message: message
		}
	}
};

export const DeleteWordRequest = (wordId) => {
	return {
		type: types.DELETE_REQUEST,
		payload: {
			wordId: wordId
		}
	}
};


export const GetWordRequest = () => {
	return {
		type: types.GETWORDS_REQUEST,
		payload: {
			isFetching: true
		}
	}
};

export const GetWordSuccess= (words) => {
	return {
		type: types.GETWORDS_SUCCESS,
		payload: {
			isFetching: false,
			words: words
		}
	}
};

export const GetWordFail = (message) => {
	return {
		type: types.GETWORDS_FAIL,
		payload: {
			isFetching: false,
			isSuccess: false,
			message: message
		}
	}
};