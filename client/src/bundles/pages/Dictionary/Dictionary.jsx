import React, { Component } from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import PropTypes from "prop-types";
import DictionaryPresenter from "./DictionaryPresenter";


class Dictionary extends Component {
	constructor(props) {
		super(props);
	}
    render() {
	    return (
		    <DictionaryPresenter dictId={this.props.dictId} option={this.props.option} path={this.props.path}/>
	    );
    }
}
Dictionary.propTypes = {
	dictId: PropTypes.string.isRequired,
	option: PropTypes.string,
	path: PropTypes.string.isRequired
};
const mapStateToProps = (state) => {
	return {
	};
};
const mapDispatchToProps = (dispatch) => {
	return bindActionCreators({
	}, dispatch);
};
export default connect(mapStateToProps, mapDispatchToProps)(Dictionary);


