const express = require('express');
const router = express.Router();

router.use('/auth', require('./auth'));
router.use('/dicts', require('./dicts'));
router.use('/words', require('./words'));

module.exports = router;