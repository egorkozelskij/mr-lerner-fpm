const express = require('express');
const router = express.Router();

router.use('/', require('./create'));
router.use('/', require('./getAll'));

module.exports = router;