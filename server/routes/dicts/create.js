const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.post('/create', wrapper(async (req, res, next) => {
	let {token, name, lang, code} = req.body;
	const validation = await validate(token);
	if (!validation.success)
		throw new Error(validation.message);
	
	const newDict = new Dict();
	newDict.userId = validation.user._id;
	newDict.name = name;
	newDict.lang = lang;
	newDict.code = code;
	await newDict.save();
	return serverSuccess({dict: newDict});
}));

module.exports = router;

