const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Word = require(path.resolve('models/Word'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.post('/create', wrapper(async (req, res, next) => {
	let {token, dictId, value, translate} = req.body;
	const validation = await validate(token);
	if (!validation.success)
		throw new Error(validation.message);
	
	const dict = await Dict.findOne({_id: dictId});
	if (!dict || dict === null || !dict.userId.equals(validation.user._id))
		throw new Error('Dictionary owner incorrect');
	
	const newWord = new Word();
	newWord.dictId = dictId;
	newWord.value = value;
	newWord.translate = translate;
	await newWord.save();
	return serverSuccess({word: newWord});
}));

module.exports = router;

