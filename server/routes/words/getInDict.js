const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Word = require(path.resolve('models/Word'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.get('/getAll', wrapper(async (req, res, next) => {
	const result = await validate(req.query.token);
	if (!result.success)
		throw new Error(result.message);
	
	const dict = await Dict.findOne({_id: req.query.dictId});
	if (!dict || dict === null || !dict.userId.equals(result.user._id))
		throw new Error('Dictionary owner incorrect');
	
	const words = await Word.find({dictId: req.query.dictId, isDeleted: false});
	return serverSuccess({words});
}));

module.exports = router;

