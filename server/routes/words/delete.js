const express = require('express');
const router = express.Router();
const path = require('path');
const validate = require(path.resolve('utils/authValidation'));
const Word = require(path.resolve('models/Word'));
const Dict = require(path.resolve('models/Dict'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.delete('/delete', wrapper(async (req, res, next) => {
	let {token, id} = req.body;
	const result = await validate(token);
	if (!result.success)
		throw new Error(result.message);
	
	const word = await Word.findOne({_id: id});
	if (!word || word === null)
		throw new Error('Error: word not found');
	
	const dict = await Dict.findOne({_id: word.dictId});
	if (!dict || !dict.userId.equals(result.user._id))
		throw new Error('Dictionary owner incorrect');
	
	word.remove();
	return serverSuccess();
}));

module.exports = router;

