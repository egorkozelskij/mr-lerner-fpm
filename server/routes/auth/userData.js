const path = require('path');
const express = require('express');
const router = express.Router();
const UserSession = require(path.resolve('models/UserSession'));
const User = require(path.resolve('models/User'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));


router.get('/getUser', wrapper( async (req, res, next) => {
	const doc = await UserSession.findOne({_id: req.query.token});
	if (!doc)
		throw new Error('Token invalid');
	
	const user = await User.findOne({_id: doc.userId}).select({"_id":0, "email":1, "signUpDate":1});
	if (!user)
		throw new Error('Token invalid');
	
	return serverSuccess({user});
}));

module.exports = router;