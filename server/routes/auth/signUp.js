const express = require('express');
const router = express.Router();
const path = require('path');
const User = require(path.resolve('models/User'));
const UserSession = require(path.resolve('models/UserSession'));
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));

router.post('/signup', wrapper(async (req, res, next) => {
	let {
		email,
		password
	} = req.body;
	
	if(!email || !password)
		throw new Error('Error: email or password cannot be blank');
	
	email = email.toLowerCase().trim();
	const users = await User.find({email: email});
	
	if (users.length > 0)
		throw new Error('Error: User already exists');
	
	const newUser = new User();
	newUser.email = email;
	newUser.password = newUser.generateHash(password);
	await newUser.save();
	
	const userSession = new UserSession();
	userSession.userId = newUser._id;
	await userSession.save();
	
	return serverSuccess({
		token: userSession._id,
		user: {
			email: newUser.email,
			signUpDate: newUser.signUpDate
		}
	});
}));

module.exports = router;