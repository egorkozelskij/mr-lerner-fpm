const path = require('path');
const express = require('express');
const router = express.Router();
const {wrapper, serverSuccess} = require(path.resolve('utils/response'));
const validate = require(path.resolve('utils/authValidation'));

router.get('/verify', wrapper( async (req, res, next) => {
	const result = await validate(req.query.token);
	if (!result.success)
		throw new Error(result.message);
	return serverSuccess({user: result.user});
}));

module.exports = router;