const mongoose = require('mongoose');

const WordSchema = new mongoose.Schema({
	dictId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'Dict'
	},
	value: {
		type: String,
		default: ''
	},
	translate: {
		type: String,
		default: ''
	},
	isDeleted: {
		type: Boolean,
		default: false
	},
	creatingDate: {
		type: Date,
		default: Date.now()
	}
});
module.exports = mongoose.model('Word', WordSchema);