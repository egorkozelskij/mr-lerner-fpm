const mongoose = require('mongoose');

const DictSchema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'User'
	},
	name: {
		type: String,
		default: ''
	},
	lang: {
		type: String,
		default: ''
	},
	code: {
		type: String,
		default: ''
	},
	creatingDate: {
		type: Date,
		default: Date.now()
	}
});
module.exports = mongoose.model('Dict', DictSchema);