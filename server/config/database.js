const path     = require('path');
const mongoose = require('mongoose');
const config   = require('./config');

module.exports = () => {
	const url = 'mongodb://'+config.db_host+':'+config.db_port+'/'+config.db_name;
	return mongoose.connect(url, { useNewUrlParser: true });
};